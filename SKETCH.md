# Structural regular expressions for Scheme

Currently a slavish applicative approximation of Sam’s
command language.

## Types

A Regexp is a SRFI 115 compiled regular expression object.

An Sregexp is an expression in the following language.


## Chain-Combinator language

rx terms are Regexps. s terms are Scheme strings. m, n, etc.
terms are non-negative integers.

### Selectors

    (x rx)      ; Select every substring matching rx.
    (y rx)      ; Select every substring not matched by rx.
    (g rx …)    ; Filters selections down to only those matching rx ….
    (v rx …)    ; Removes selections matching rx ….
    (lines m n) ; Select a line range of the input (m ≤ n).
    (chars m n) ; Select a codepoint range of the input (m ≤ n).

### Modifiers

    d      ; Delete every selected substring.
    (a s)  ; Append string s after every substring.
    (i s)  ; Insert string s before every substring.
    (c s)  ; Replace every substring with string s.

## Procedures

(sregexp (List-of Sregexp)) → Procedure

Takes a list of Sregexp expressions and returns a String → String
procedure which transforms strings as described by the expression.
It is an error if the last element of the Sregexp-list argument is
not a modifier.

Example:

    (let ((transformer (sregexp '((x (+ digit alpha))
                                  (x (+ alpha))
                                  (c "Z")))))
      (transformer "1a3 4b5 67 c89"))

      ⇒ "1Z3 4Z5 67 c89"

## Syntax

(sregexp-transform String Sregexp …) → String

Apply the transformation described by the given sregexps, or
return the string unchanged if there are no sregexp arguments. Each
structural regexp operation is applied to all of the results of the
previous operation; operations are applied from left to right.
The final sregexp argument must be a modifier. Sregexps are
implicitly quasiquoted.

Example:

    (sregexp-transform "1a3 4b5 67 c89"
                       (x (+ digit alpha))
                       (x (+ alpha))
                       (c "Z"))

      ⇒ "1Z3 4Z5 67 c89"


    (sregexp-transform "1a3 4b5 67 c89"
                       (y (+ white))
                       (i "Z"))

      ⇒ "Z1a3 Z4b5 Z67 Zc89"


    (sregexp-transform "1a3 4b5 67 c89"
                       (y (+ white))
                       (i "Z"))

      ⇒ "Z1a3 Z4b5 Z67 Zc89"


    (sregexp-transform "1a3 4b5 67 c89"
                       (x (+ digit alpha))
                       (x (+ alpha))
                       d)

      ⇒ "13 45 67 c89"
